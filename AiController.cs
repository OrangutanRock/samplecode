﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiController : MonoBehaviour
{
    private List<AiBehavior> Ais = new List<AiBehavior>();
    public float baseActionDelay = 0.1f;
    private float actionDelay = 0.1f;
    private Troop troop;
    private bool keepUpdating;
    private Dictionary<SkillName, System.Type> dic = new Dictionary<SkillName, System.Type>();


    IEnumerator Start()
    {
        troop = GetComponent<Troop>();
        yield return new WaitUntil(() => troop?.PrimateInfo()?.UnitInfo()?.BattleInfo() != null);
        if (troop != null)
        {
            yield return AddTroopAis();
            troop.PrimateInfo().typeChanged.AddListener(() => StartCoroutine(AddTroopAis()));
            troop.PrimateInfo().skillLearned.AddListener(() => StartCoroutine(AddTroopAis()));
        }
        BattleManager.Current.EventManager().AddListener(EventType.BATTLE_ENDED, Stop);
        //Invoke("NextMove", 1f);
        keepUpdating = true;
        StartCoroutine(NextMove());
    }


    private void Stop()
    {
        keepUpdating = false;
    }


    private AiBehavior Ai(System.Type aiType)
    {
        var ai = GetComponent(aiType) as AiBehavior;
        if (ai == null)
            ai = gameObject.AddComponent(aiType) as AiBehavior;
        return ai;
    }

    private IEnumerator AddAi(Skill skill, bool automate)
    {
        var type = skill.AiType();
        if (type == null) yield break;

        var ai = Ai(type);
        ai.SetSkill(skill);
        if (automate)
            AddAi(ai);
        yield return new WaitUntil(ai.IsReady);
    }

    private IEnumerator AddAi(System.Type aiType, bool automate)
    {
        if (aiType == null) yield break;

        var ai = Ai(aiType);
        if (automate)
            AddAi(ai);
        yield return new WaitUntil(ai.IsReady);
    }



    //private void AddAi<T> (bool automate) where T : AiBehavior
    //{
    //    AiBehavior ai = GetComponent<T>();
    //    if (ai == null)
    //        ai = gameObject.AddComponent<T>();
    //    if (automate)
    //      AddAi(ai);
    //}

    private IEnumerator AddTroopAis()
    {
        Ais.Clear();
        yield return StartCoroutine(AddAi(typeof(AttackAi), true));
        yield return StartCoroutine(AddAi(typeof(FindTargetAi), true));
        yield return StartCoroutine(AddAi(typeof(MoveTowardsTargetAi), true));
        yield return StartCoroutine(AddAi(typeof(DoNothingAi), true));

        var skills = troop.PrimateInfo().Skills();

        for (int i = 0; i < skills.Count; i++)
        {
           yield return StartCoroutine(AddAi(skills[i], troop.player.IsAi()));
        }
    }

    public void AddAi(AiBehavior behavior)
    {
        Ais.Add(behavior);
    }

    public bool IsAuto(AiBehavior behavior)
    {
        return Ais.Contains(behavior);
    }

    public void RemoveAi(AiBehavior behavior)
    {
        Ais.Remove(behavior);
    }


    // Update is called once per frame
    private IEnumerator NextMove()
    {
        while (keepUpdating)
        {
            float bestWeight = float.MinValue;
            AiBehavior bestAi = null;
            for (int i = 0; i < Ais.Count; i++)
            {
                var ai = Ais[i];
                if (!ai.enabled || troop == null) continue;
                var weight = ai.GetWeight();
                // Debug.Log(GetComponent<Troop>().Name() + " " + ai.aiName() + ' ' + weight);
                if (weight > bestWeight)
                {
                    bestWeight = weight;
                    bestAi = ai;
                }
            }

            if (bestWeight > 0)
            {
                for (int i = 0; i < Ais.Count; i++)
                {
                    var ai = Ais[i];
                    var bestAiName = bestAi.aiName();
                    if (ai != bestAi) // && !(bestAiName.Equals("Find target")))// || bestAiName.Equals("Move")))
                    {
                        ai.NotExecute();
                        //Debug.Log("DBG " + bestAiName);
                    }
                }
                bestAi.Execute();
                actionDelay = bestAi.Delay();
            }
            else
            {
                actionDelay = baseActionDelay;
                if (bestAi != null)
                    bestAi.NotExecute();
            }
            //        Debug.Log(GetComponent<Troop>().Name() + " " + bestWeight + " " + bestAi.aiName() + " " + actionDelay);
            if (troop != null && bestAi != null)
                Debug.Log(troop.Name() + " " + bestAi.aiName() + " " + bestWeight);

            yield return new WaitForSeconds(actionDelay);
        }
    }
}
