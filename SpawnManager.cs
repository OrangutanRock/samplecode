﻿using System.Collections;
using System.Collections.Generic;
//using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SpawnManager : MonoBehaviour
{
    private PlayerManager player;

    private bool keepUpdating;

    private TribeType type;

    public bool autoSet = true;
    public bool randomEnemy = true;

    public BattleInfo MonkeyTemplate { get; set; }
    public float SpawnDelay { get; set; } = 0;
    public float SpawnRate { get; set; } = 20f;

    public float Value()
    {
        return MonkeyTemplate.GetValue();
    }

    private void Awake()
    {
        BattleManager.Current.localPlayerReady.AddListener(() => StartCoroutine(Init()));
    }

    public void SetBuilding(BuildingInfo info)
    {
        MonkeyTemplate = info.SpawnUnitTemplate;
        SpawnDelay = info.SpawnDelay;
        SpawnRate = info.SpawnRate;
        keepUpdating = true;
    }

    public IEnumerator Init()
    {
        Debug.Log("LocalPlayerReady Init");
        player = BattleManager.Current.playerAi;
        type = player.Type();
        Debug.Log("SpawnManager " + type.ToString());

        if (autoSet)
        {
            keepUpdating = true;
            var enemyCount = player.Enemy.Troops().Count();
            MonkeyTemplate = player.Enemy.Troops()[Random.Range(0, enemyCount)].GetComponent<Troop>().PrimateInfo().UnitInfo().BattleInfo();
        }

        BattleManager.Current.EventManager().AddListener(EventType.BATTLE_ENDED, Stop);

        yield return new WaitUntil(() => keepUpdating);
        yield return new WaitForSeconds(SpawnDelay);
        StartCoroutine(UpdateSpawnRateRegularly());
        StartCoroutine(SpawnRegularly());

    }

    private void Stop() =>
        keepUpdating = false;

    private void UpdateSpawnRate()
    {
        SpawnRate *= BattleMap.Current.SpawnRateIncrease();
        //Invoke("UpdateSpawnRate", BattleMap.Current.spawnRateTime);
    }

    private IEnumerator UpdateSpawnRateRegularly()
    {
        while (keepUpdating)
        {
            yield return new WaitForSeconds(BattleMap.Current.SpawnRateTime());
            UpdateSpawnRate();
        }
    }

    private void Spawn()
    {
        if (MonkeyTemplate == null) return;
        var monkey = new UnitInfo(type);


        if (randomEnemy)
        {
            monkey.BattleInfo().CopyCharacteristics(MonkeyTemplate.CreateEnemy());
        }
        else
        {
            monkey.BattleInfo().CopyCharacteristics(MonkeyTemplate);
        }
        Debug.Log("SpawnManager spawning" + monkey.BattleInfo().Type().ToString());

        player.TroopManager().AddTroop(transform.position, monkey);
        //Invoke("Spawn", spawnRate);
    }

    private IEnumerator SpawnRegularly()
    {
        while (keepUpdating)
        {
            Spawn();
            yield return new WaitForSeconds(SpawnRate);
        }
    }






}


