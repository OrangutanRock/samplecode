﻿using System.Collections.Generic;
using UnityEngine;

   
public class ProjectileManager : MonoBehaviour
{
    public GameObject projectilePrefab;
    private List<ProjectileInfo> projectilePool = new List<ProjectileInfo>();
    public static ProjectileManager Current = null;

    void Awake()
    {
        if (Current)
        {
            Destroy(this.gameObject);
            return;
        }
        Current = this;
    }

    public void AddProjectile(Unit unit, float damage, ProjectileName projectileName, Sprite projectileSprite)
    {
        ProjectileInfo projectile = null;
        if (projectilePool.Count > 0)
        {
            projectile = projectilePool[0];
            projectilePool.RemoveAt(0);
        }
        else
        {
            projectile = Instantiate(projectilePrefab).GetComponent<ProjectileInfo>();
        }

        projectile.SetUp(unit, damage, projectileName, projectileSprite);
    }

    public void DestroyProjectile(ProjectileInfo projectile)
    {
        projectile.gameObject.SetActive(false);
        projectilePool.Add(projectile);
    }

}
