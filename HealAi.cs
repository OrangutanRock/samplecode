﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealAi : AiBehavior
{

    public override string aiName() { return "Heal"; }

    public override float GetWeight()
    {
        if (!CanUse())
            return 0;

        return Mathf.Min(MaxHealing() / skillInfo.ValueModifier, 1f);
    }

    public float MaxHealing()
    {
        target = troop;
        var troopDM = troop.GetComponent<DamageManager>();
        float maxHealing = troopDM.MaxHealth() - troopDM.CurrentHealth();

        var alliesInRange = troop.VisibilityManager().Allies().WithoutEffect(SkillName.HEALING);


        for (int i = 0; i < alliesInRange.Count(); i++)
        {
            var allyDM = alliesInRange[i].GetComponent<DamageManager>();
            var currHealing = allyDM.MaxHealth() - allyDM.CurrentHealth(); // Heal the most in danger.
            if (currHealing > maxHealing)
            {
                maxHealing = currHealing;
                target = alliesInRange[i].GetComponent<Unit>();
            }
        }

        return maxHealing;
    }

    public override bool NeedInput()
    {
        return target == null;
    }

    public override bool UseInput(GameObject inputObject)
    {
        var inputUnit = inputObject.GetComponent<Unit>();
        if (inputUnit == null) return false;

        if (inputUnit.player != troop.player
        || inputUnit.GetComponent<Troop>() == null
        || inputUnit.GetComponent<DamageManager>() == null
            || Config.Distance(inputUnit, troop) > skillInfo.Range)
            return false;

        target = inputUnit;
        return true;
    }



    public override void Execute()
    {
        if (!CanUse())
            return;

        Debug.Log("Healing");

        timePassed = 0f;

        // Apply effect to selected target
        EffectTarget();
       
        //Debug.Log(unit.Name() + " heals " + target.Name());
    }
        



  
}
